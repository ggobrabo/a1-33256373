import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        String csvFile = "alunos.csv";
        String line = "";
        String csvSplitBy = "\t";
        List<Aluno> listaDeAlunos = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            while ((line = br.readLine()) != null) {
                
                if (line.startsWith("Matrícula")) {
                    continue;
                }
                
                String[] dados = line.split(csvSplitBy);
                String matricula = dados[0];
                String nome = dados[1];
                double nota = Double.parseDouble(dados[2].replace(",", ".")); // Tratar vírgula como separador decimal

                Aluno aluno = new Aluno(matricula, nome, nota);
                listaDeAlunos.add(aluno);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        
        if (!listaDeAlunos.isEmpty()) {
          
            int totalAlunos = listaDeAlunos.size();
            int aprovados = 0;
            int reprovados = 0;
            double menorNota = Double.MAX_VALUE;
            double maiorNota = Double.MIN_VALUE;
            double somaNotas = 0.0;

        
            for (Aluno aluno : listaDeAlunos) {
                
                if (aluno.getNota() >= 6.0) {
                    aprovados++;
                } else {
                    reprovados++;
                }

                
                if (aluno.getNota() < menorNota) {
                    menorNota = aluno.getNota();
                }
                if (aluno.getNota() > maiorNota) {
                    maiorNota = aluno.getNota();
                }

                
                somaNotas += aluno.getNota();
            }

            
            double mediaGeral = somaNotas / totalAlunos;

            
            String resumoFile = "resumo.csv";
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(resumoFile))) {
                bw.write("Quantidade de alunos na turma," + totalAlunos + "\n");
                bw.write("Quantidade de alunos aprovados," + aprovados + "\n");
                bw.write("Quantidade de alunos reprovados," + reprovados + "\n");
                bw.write("Menor nota da turma," + menorNota + "\n");
                bw.write("Maior nota da turma," + maiorNota + "\n");
                bw.write("Média geral da turma," + mediaGeral + "\n");

                System.out.println("Arquivo resumo.csv gerado com sucesso!");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Não há alunos para processar.");
        }
    }
}
